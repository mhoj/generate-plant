
const TAU = Math.PI * 2;
const TAU_DEG = TAU / 360;
const RATIO = 0.5 || window.devicePixelRatio || 1;
let COLORS = null;

// function setup() {
//     COLORS = [
//     color(255, 68, 68), color(187, 34, 68), color(255, 187, 170), color(68, 85, 85),
//     color(51, 51, 58), color(238, 238, 238), color(80, 56, 51),color(153, 153, 51),
//     color(80, 140, 150), color(200, 140, 10), color(110, 95, 80), color(10, 45, 20)];

//     createCanvas(160, 160);
//     background(0,0,0);

//     const plant = new Plant(80,150, {});
//     plant.generate();
//     plant.draw();
// }

const genRandomFloat = (min, max) => (Math.random() * (max - min) + min);

const genRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const byPercent = (p) => (Math.random() * 100 < p);

const constrain = (val, min, max) => (val < max ? (val > min ? val : min) : max);


function genRandomParam() {
    return {
        stemLength: genRandomInt(15,30),
        thickness: genRandomInt(2,4),
        angle: Math.PI * genRandomFloat(1.4, 1.6),
        flowering: byPercent(70),
        energy: genRandomInt(15, 30),
        curveLimit: genRandomFloat(4, 9),
        spurRate: genRandomInt(30, 50),
        splitRate: genRandomInt(5, 30),
        flowerRate: genRandomInt(10, 40),
        flowerSize: genRandomInt(2, 8),
        flowerNum : 4,
        colorStem: genRandomInt(0, COLORS.length - 1),
        colorStemDark: genRandomInt(0, COLORS.length - 1),
        colorFlower: genRandomInt(0, COLORS.length - 1),
    }
}

function mutateParam(param) {
    return {
        stemLength: constrain(param.stemLength + genRandomInt(-5,5), 15, 30),
        thickness: constrain(param.thickness + genRandomInt(-1,1), 2, 5),
        angle: param.angle,
        flowering: param.flowering || byPercent(10) && !byPercent(10),
        energy: constrain(param.energy + genRandomInt(-5,5), 15, 30),
        curveLimit: constrain(param.curveLimit + genRandomInt(-1,1), 4, 9),
        spurRate: constrain(param.spurRate + genRandomInt(-5,5), 30, 50),
        splitRate: constrain(param.splitRate + genRandomInt(-5,5), 0, 50),
        flowerRate: constrain(param.flowerRate + genRandomInt(-5,5), 0, 50),
        flowerSize: constrain(param.flowerSize + genRandomInt(-1, 1), 2, 10),
        flowerNum : 4,
        colorStem: byPercent(95) ? param.colorStem : genRandomInt(0, COLORS.length - 1),
        colorStemDark: byPercent(95) ? param.colorStemDark : genRandomInt(0, COLORS.length - 1),
        colorFlower: byPercent(95) ? param.colorFlower : genRandomInt(0, COLORS.length - 1),
    }
}

function mixFactor(a, b) {
    if (byPercent(10)) { return a; }
    if (byPercent(10)) { return b; }
    const rnd = Math.random();
    return a * rnd + b * (1 - rnd);
}

function crossoverParam(param1, param2) {
    const crossed = {
        stemLength: mixFactor(param1.stemLength, param2.stemLength),
        thickness: mixFactor(param1.thickness, param2.thickness),
        angle: mixFactor(param1.angle, param2.angle),
        flowering: byPercent(50) ? param1.flowering : param2.flowering,
        energy: mixFactor(param1.energy, param2.energy),
        curveLimit: mixFactor(param1.curveLimit, param2.curveLimit),
        spurRate: mixFactor(param1.spurRate, param2.spurRate),
        splitRate: mixFactor(param1.splitRate, param2.splitRate),
        flowerRate: mixFactor(param1.flowerRate, param2.flowerRate),
        flowerSize: mixFactor(param1.flowerSize, param2.flowerSize),
        flowerNum : 4,
        colorStem: byPercent(50) ? param1.colorStem : param2.colorStem,
        colorStemDark: byPercent(50) ? param1.colorStemDark : param2.colorStemDark,
        colorFlower: byPercent(50) ? param1.colorFlower: param2.colorFlower,
    }
    return mutateParam(crossed);
}

class Plant {
    constructor (x, y, param, layer) {
        const defaults = {
            stemLength: 30,
            thickness: 3,
            angle: Math.PI * 1.5,
            flowering: 1,
            energy: genRandomInt(15, 30),
            curveLimit: genRandomFloat(4, 9),
            spurRate: 45,
            splitRate: 15,
            flowerRate: 15,
            flowerSize: 7,
            mainStemNum: 5,
            flowerNum: 4,
            colorStem: genRandomInt(0, COLORS.length - 1),
            colorStemDark: genRandomInt(0, COLORS.length - 1),
            colorFlower: genRandomInt(0, COLORS.length - 1),
        }
        for (const key in defaults) {
            param[key] = param[key] || defaults[key];
        }
        this.param = param;
        this.x = 0;
        this.y = 0;
        this.offset = {x: x, y: y};
        this.colPerc = 0;
        this.stemLength = param.stemLength;
        this.thickness = param.thickness;
        this.angle = param.angle;
        this.flowering = param.flowering;
        this.energy = param.energy;
        this.curveLimit = param.curveLimit;
        this.splitRate = param.splitRate;
        this.spurRate = param.spurRate;
        this.flowerRate = param.flowerRate;
        this.flowerSize = param.flowerSize;
        this.mainStemNum = param.mainStemNum;
        this.flowerNum = param.flowerNum;
        this.colorStem = param.colorStem;
        this.colorStemDark = param.colorStemDark;
        this.colorFlower = param.colorFlower;

        this.stems = [];
        this.flowers = [];
        this.layer = layer;
    }

    setOffset(offset) {
        this.offset = {x: offset[0], y: offset[1]};
    }

    setLayer(layer) { this.layer = layer; } 

    generate() {
        for (let i = 0; i < this.mainStemNum; i++) {
            const param = {
                angle: this.angle + TAU_DEG * genRandomInt(-30, 30),
                mag: 1,
                energy: this.energy,
                heading: null,
                colPerc: this.colPerc,
                shadow: genRandomFloat(30, 90)
            }
            this.stems.push( new Stem(this, this.x, this.y, param));
        }

    }

    draw() {
        // translate canvas
        if (this.layer) {
            this.layer.translate(this.offset.x, this.offset.y);
        } else {
            translate(this.offset.x, this.offset.y);
        }
        // draw
        for (const stem of this.stems) {
            stem.draw();
        }
        for (const flower of this.flowers) {
            flower.draw();
        }
        // restore canvas
        if (this.layer) {
            this.layer.translate(-this.offset.x, -this.offset.y);
        } else {
            translate(this.offset.x, this.offset.y);
        }
    }
}

class Stem {
    constructor(plant, x, y, param) {
        this.plant = plant;
        this.x = x;
        this.y = y;
        this.angle = param.angle;
        this.heading = param.heading || TAU_DEG * genRandomInt(-10, 10);
        this.headingDest = this.heading; ///
        this.mag = param.mag;
        this.thickness = this.plant.thickness * genRandomFloat(0.5,1) * param.mag * RATIO;
        this.energy = param.energy;
        this.vector = this.setAngle(this.angle);
        this.colPerc = constrain(param.colPerc + random(-0.1, 0.2), 0, 1);
        this.shadow = Math.max(param.shadow - 3, 0);
        this.setup();
    }

    setup() {
        if (this.energy > 0) {
            this.mag *= 0.97;
            if (byPercent(this.plant.splitRate)) {
                this.split();
            }
            else if (byPercent(this.plant.spurRate)) {
                this.spur();
            }
            else {
                this.continue();
            }
        } else if (this.plant.flowering && byPercent(this.plant.flowerRate)) {
            this.flower();
        }
    }

    spur() {
        this.continue();

        const r = 50;
        const polarity = genRandomInt(0, 1) ? -1 : 1;
        for (let i = 0; i < 2; i++) {
            const param = {
                angle: this.angle - (TAU_DEG * genRandomInt(-r/5, r) * polarity),
                mag: this.mag * 0.7,
                energy: this.energy * 0.15,
                heading: this.heading + genRandomInt(-1, 1),
                colPerc: this.colPerc,
                shadow: this.shadow
            }
            this.plant.stems.push(new Stem(this.plant, this.x + this.vector.x, this.y +  this.vector.y, param));
        }
    }

    split() {
        this.energy -= 3;

        const r = 20;
        const polarity = genRandomInt(0, 1) ? -1 : 1;
        for (let i = 0; i < 2; i++) {
            const param = {
                angle: this.angle - (TAU_DEG * genRandomInt(-r/5, r) * polarity),
                mag: this.mag,
                energy: this.energy,
                heading: this.heading + genRandomInt(-1, 1),
                colPerc: this.colPerc,
                shadow: this.shadow
            }
            this.plant.stems.push(new Stem(this.plant, this.x + this.vector.x, this.y +  this.vector.y, param));
        }
    }
    
    continue() {
        const r = byPercent(10) ? 25 : 7;
        this.energy -= 1;
        const param = {
            angle: this.angle + (TAU_DEG * genRandomFloat(-r, r)),
            mag: this.mag,
            energy: this.energy,
            heading: this.heading,
            colPerc: this.colPerc,
            shadow: this.shwdow
        }
        this.plant.stems.push(new Stem(this.plant, this.x + this.vector.x, this.y + this.vector.y, param));
    };

    flower() {
        const param = {
            angle: this.angle,
            colPerc: this.colPerc
        }
        this.plant.flowers.push(new Flower(this.plant, this.x + this.vector.x, this.y + this.vector.y, param));
    }

    setAngle() {
        const cone = TAU_DEG * 30;
        const dir = this.plant.angle;
        
        if (this.angle < (dir - cone)) {
          this.angle += (TAU_DEG * 0.5);
          this.heading += (TAU_DEG * genRandomInt(1, 2));
        }
        else if (this.angle > (dir + cone)) {
          this.angle -= (TAU_DEG * 0.5);
          this.heading -= (TAU_DEG * genRandomInt(1, 2));
        }
        else {
          this.heading += (TAU_DEG * genRandomInt(-1, 1));
        }
       
        this.heading = constrain(this.heading, -TAU_DEG * this.plant.curveLimit, TAU_DEG * this.plant.curveLimit);
        this.angle += this.heading;

        const length = this.plant.stemLength * genRandomFloat(0.7,1) * this.mag * RATIO;
        return p5.Vector.fromAngle(this.angle, length);
    }

    draw() {
        const c = lerpColor(COLORS[this.plant.colorStem], 
            COLORS[this.plant.colorStemDark], this.colPerc);
        if (this.plant.layer) {
            const layer = this.plant.layer;
            layer.strokeWeight(this.thickness);
            layer.stroke(c);
            layer.line(this.x, this.y, this.x + this.vector.x, this.y + this.vector.y);
        } else {
            strokeWeight(this.thickness);
            stroke(c);
            line(this.x, this.y, this.x + this.vector.x, this.y + this.vector.y);
        }

    }
}

class Flower {
    constructor(plant, x, y, param) {
        this.plant = plant;
        this.x = x;
        this.y = y;
        this.angle = param.angle;
        this.colPerc = param.colPerc;
        this.size = this.plant.flowerSize * genRandomFloat(0.6, 1,5) * RATIO;
    }

    draw() {
        if (this.plant.layer) {
            const layer = this.plant.layer;
            layer.translate(this.x, this.y);
            layer.rotate(this.angle - (TAU/4));
            const x = [-this.size * 0.8, this.size * 0.8, 0, 0];
            const y = [0, 0, - (this.size * 0.4), this.size * 0.4];
            const c = lerpColor(COLORS[this.plant.colorFlower],
                COLORS[this.plant.colorStemDark], this.colPerc / 2);
            layer.noStroke();
            layer.fill( c );
            for (let i = 0; i < 4; i++) {
                layer.circle(x[i], y[i], this.size);
            }
            layer.rotate(-this.angle + (TAU/4));
            layer.translate(-this.x, -this.y);
        } else {
            translate(this.x, this.y);
            rotate(this.angle - (TAU/4));
    
            const x = [-this.size * 2.5, this.size * 2.5, 0, 0];
            const y = [0, 0, - (this.size * 0.6), this.size * 0.6];
            const c = lerpColor(COLORS[this.plant.colorFlower],
                COLORS[this.plant.colorStemDark], this.colPerc / 2);
            noStroke();
            fill( c );
            for (let i = 0; i < 4; i++) {
                circle(x[i], y[i], this.size);
            }
            rotate(-this.angle + (TAU/4));
            translate(-this.x, -this.y);
        }
    }
}