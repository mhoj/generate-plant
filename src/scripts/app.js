const CONFIG = {
    width: 900,
    gardenWidth: 600,
    height: 600,
    gridSize: 120,
    gridNum: [3, 4],
    optSize: [180, 240]
}
CONFIG.gridWidth = CONFIG.gridSize * CONFIG.gridNum[0];
CONFIG.gridHeight = CONFIG.gridSize * CONFIG.gridNum[1];
CONFIG.gridOffset = [
    (CONFIG.gardenWidth - CONFIG.gridSize * CONFIG.gridNum[0]) / 2,
    (CONFIG.height - CONFIG.gridSize * CONFIG.gridNum[1]) / 2  
];
CONFIG.optOffest = [
    CONFIG.gardenWidth + (CONFIG.width - CONFIG.gardenWidth - CONFIG.optSize[0]) / 2,
    (CONFIG.height - CONFIG.optSize[1]) / 1.5
];
CONFIG.optPlantOffest = [
    CONFIG.optOffest[0] + CONFIG.optSize[0] / 2,
    CONFIG.optOffest[1] + CONFIG.optSize[1] * 0.9
];
// state //
const STATES = {
    SELECT: 'select',
    PLANT: 'plant'
};
let state = STATES.SELECT;

// layers and assets //
let plantLayer, genPlantLayer, gridLayer, highlightLayer, selectedLayer;
// plant
const plants = [];
let generatedPlant = null;
// highlight
let gridHighlight = null;
let optSelected = false;
const gridSelected = [];
// buttons
let resetButton, genButton;
// audio
const bgmAudio = document.getElementById('bgmaudio');
let bgmPlaying = false;

// utilitys //
function getPlantOffest(i, j) {
    return [CONFIG.gridOffset[0] + i * CONFIG.gridSize + CONFIG.gridSize / 2,
            CONFIG.gridOffset[1] + j * CONFIG.gridSize + CONFIG.gridSize * 0.9];
}

function getGridOffest(i, j) {
    return [CONFIG.gridOffset[0] + i * CONFIG.gridSize,
            CONFIG.gridOffset[1] + j * CONFIG.gridSize];
}

function getIndexFromCord(i, j) {
    return i * CONFIG.gridNum[1] + j;
}

// P5 control //
function setup() {
    COLORS = [
    color(255, 68, 68), color(187, 34, 68), color(255, 187, 170), color(68, 85, 85),
    color(51, 51, 58), color(238, 238, 238), color(80, 56, 51),color(153, 153, 51),
    color(80, 140, 150), color(200, 140, 10), color(110, 95, 80), color(10, 45, 20)];

    createCanvas(CONFIG.width, CONFIG.height);
    
    plantLayer = createGraphics(CONFIG.width, CONFIG.height);
    genPlantLayer = createGraphics(CONFIG.width, CONFIG.height);
    highlightLayer = createGraphics(CONFIG.width, CONFIG.height);
    selectedLayer = createGraphics(CONFIG.width, CONFIG.height);
    gridLayer = createGraphics(CONFIG.width, CONFIG.height);
    gridLayer.background(0,0,0);
    
    drawGrids();
    //image(gridLayer, 0, 0);

    initPlants();
    //image(plantLayer, 0, 0);
    createButtons();
    setupAudio();
}

function draw() {
    // reset
    fill(color(0, 0, 0));
    rect(0, 0, CONFIG.width, CONFIG.height);
    image(gridLayer, 0, 0);

    // highlight
    highlightLayer.clear();
    const cord = ifInsideGrid(mouseX, mouseY);
    if (cord === false) {
        gridHighlight = null;
    } else {
        gridHighlight = cord;
        highlightLayer.noFill();
        if (state === STATES.PLANT) {
            highlightLayer.fill('rgba(100,100,100,0.6)');
        }
        highlightLayer.stroke(color(100, 100, 100));
        highlightLayer.strokeWeight(2);
        const pos = getGridOffest(cord[0], cord[1]);
        highlightLayer.rect(pos[0], pos[1], CONFIG.gridSize, CONFIG.gridSize);
    }
    if (state === STATES.PLANT || ifInsideOpt(mouseX, mouseY)) {
        highlightLayer.noFill();
        highlightLayer.stroke(color(100, 100, 100));
        if (state === STATES.PLANT) {
            highlightLayer.stroke(color(180, 180, 180));
        }
        highlightLayer.strokeWeight(2);
        highlightLayer.rect(CONFIG.optOffest[0],
            CONFIG.optOffest[1],
            CONFIG.optSize[0],
            CONFIG.optSize[1]
        );0
    }
    image(highlightLayer, 0, 0);

    // selection
    drawSelected();
    image(selectedLayer, 0, 0);

    // plants
    image(genPlantLayer, 0, 0);
    image(plantLayer, 0, 0);
}

function removeSelected(cord) {
    for (let i = 0; i < gridSelected.length; i++) {
        if (gridSelected[i][0] === cord[0]
            && gridSelected[i][1] === cord[1]) {
                if (i == 0) { gridSelected.shift(); }
                else { gridSelected.pop(); }
                return true;
         }
    }
    return false;
}

function mouseClicked() {
    if (!bgmPlaying) {
        bgmPlaying = true;
        bgmAudio.play();
    }
    if (ifInsideOpt(mouseX, mouseY) && generatedPlant) {
        state = STATES.PLANT;
        return;
    }
    const cord = ifInsideGrid(mouseX, mouseY);
    if (cord === false) {
        if (mouseX < CONFIG.gardenWidth) {
            while(gridSelected.length) { gridSelected.pop(); }
        }
        state = STATES.SELECT; 
        return;
    }
    switch(state) {
        case STATES.SELECT: {
            const id = getIndexFromCord(cord[0], cord[1]);
            if (!plants[id]) { return; }
            if(removeSelected(cord)) { return; }
            if (gridSelected.length == 2) { gridSelected.shift(); }
            gridSelected.push(cord);
            break;
        }
        case STATES.PLANT: {
            putPlantAt(cord);
            break;
        }
    }
    state = STATES.SELECT;
}

function doubleClicked() {
    const cord = ifInsideGrid(mouseX, mouseY);
    if (cord === false) { return; }
    if (state !== STATES.SELECT) { return; }
    const id = getIndexFromCord(cord[0], cord[1]);
    if (!plants[id]) { return; }
    removeSelected(cord);
    plants[id] = null;
    drawPlants();
}

// Audio //
function setupAudio() {
    bgmAudio.volume = 0.1;
    bgmAudio.muted = false;
}

// Button //
function createButtons() {
    resetButton = createDiv('reset');
    resetButton.class('button');
    resetButton.position(CONFIG.optOffest[0] - 10, CONFIG.optOffest[1] - 180);
    resetButton.mousePressed(resetPlants);

    genButton = createDiv('generate');
    genButton.class('button');
    genButton.position(CONFIG.optOffest[0] - 10, CONFIG.optOffest[1] - 100);
    genButton.mousePressed(generatePlant);
}

// Grids //
function ifInsideGrid(x, y) {
    const dx = x - CONFIG.gridOffset[0],
          dy = y - CONFIG.gridOffset[1];
    if (dx < 0 || dx >= CONFIG.gridWidth
        || dy < 0 || dy >= CONFIG.gridHeight) { return false; }
    return [Math.floor(dx / CONFIG.gridSize),
        Math.floor(dy / CONFIG.gridSize) ];
}

function ifInsideOpt(x, y) {
    const dx = x - CONFIG.optOffest[0],
          dy = y - CONFIG.optOffest[1];
    return (dx >= 0 && dy >= 0 && dx < CONFIG.optSize[0] && dy < CONFIG.optSize[1]);
}

function drawGrids() {
    gridLayer.stroke(color(100, 100, 100));
    gridLayer.strokeWeight(1);
    for (let i = 0, p = 0; i <= CONFIG.gridNum[0]; i++, p += CONFIG.gridSize) {
        gridLayer.line(CONFIG.gridOffset[0] + p,
                        CONFIG.gridOffset[1],
                        CONFIG.gridOffset[0] + p,
                        CONFIG.gridOffset[1] + CONFIG.gridHeight);
    }
    for (let i = 0, p = 0; i <= CONFIG.gridNum[1]; i++, p += CONFIG.gridSize) {
        gridLayer.line(CONFIG.gridOffset[0],
                        CONFIG.gridOffset[1] + p,
                        CONFIG.gridOffset[0] + CONFIG.gridWidth,
                        CONFIG.gridOffset[1] + p);
    }
    gridLayer.line(CONFIG.gardenWidth, 0, CONFIG.gardenWidth, CONFIG.height);
    gridLayer.fill(0, 0, 0);
    gridLayer.rect(CONFIG.optOffest[0],
        CONFIG.optOffest[1],
        CONFIG.optSize[0],
        CONFIG.optSize[1]
    );
}

// Selected //
function drawSelected() {
    selectedLayer.clear();
    if (gridSelected.length === 0) { return; }
    for (const cord of gridSelected) {
        selectedLayer.noFill();
        selectedLayer.stroke(color(180, 180, 180));
        selectedLayer.strokeWeight(2);
        const pos = getGridOffest(cord[0], cord[1]);
        selectedLayer.rect(pos[0], pos[1], CONFIG.gridSize, CONFIG.gridSize);
    }
}

// Plants //
function initPlants() {
    for (let i = 0; i < CONFIG.gridNum[0]; i++) {
        for (let j = 0; j < CONFIG.gridNum[1]; j++) {
            if (Math.random() < 0.5) { plants.push(null); continue; }
            const pos = getPlantOffest(i, j);
            const plant = new Plant(pos[0], pos[1], genRandomParam(), plantLayer);
            plant.generate();
            plant.draw();
            plants.push(plant);
        }
    }
}

function resetPlants() {
    while(gridSelected.length) { gridSelected.pop(); }
    while(plants.length) { plants.pop(); }
    plantLayer.clear();
    initPlants();
}

function generatePlant() {
    genPlantLayer.clear();
    switch (gridSelected.length) {
        case 0: {
            generatedPlant = new Plant(
                CONFIG.optPlantOffest[0], CONFIG.optPlantOffest[1], genRandomParam(), genPlantLayer);
            break;
        }
        case 1: {
            const selected = plants[getIndexFromCord(gridSelected[0][0], gridSelected[0][1])]; 
            generatedPlant = new Plant(
                CONFIG.optPlantOffest[0], CONFIG.optPlantOffest[1], mutateParam(selected.param), genPlantLayer);
            break;
        }
        case 2: {
            const selected1 = plants[getIndexFromCord(gridSelected[0][0], gridSelected[0][1])];
            const selected2 = plants[getIndexFromCord(gridSelected[1][0], gridSelected[1][1])];
            generatedPlant = new Plant(
                CONFIG.optPlantOffest[0], CONFIG.optPlantOffest[1],
                crossoverParam(selected1.param, selected2.param), genPlantLayer);
            break;
        }
    }
    generatedPlant.generate();
    generatedPlant.draw();
}

function putPlantAt(cord) {
    const id = getIndexFromCord(cord[0], cord[1]);
    //const plant = Object.assign(Object.create(Object.getPrototypeOf(generatedPlant)), generatedPlant);
    const plant = generatedPlant;
    plant.setOffset(getPlantOffest(cord[0], cord[1]));
    plant.setLayer(plantLayer);
    plants[id] = plant;
    drawPlants();
    generatedPlant = null;
    genPlantLayer.clear();
}


function drawPlants() {
    plantLayer.clear();
    for(const plant of plants) {
        if (plant) { plant.draw(); }
    }
}

function drawGeneratedPlant() {
    generatedPlant.draw();
}